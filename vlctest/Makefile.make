all:
	gcc vlctest.cpp -o vlctest -l vlc -L/Applications/VLC.app/Contents/MacOS/lib -I/Applications/VLC.app/Contents/MacOS/include
	DYLD_LIBRARY_PATH=/Applications/VLC.app/Contents/MacOS/lib ./vlctest

run:
	DYLD_LIBRARY_PATH=/Applications/VLC.app/Contents/MacOS/lib ./vlctest
