import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.0
import Controller 1.0
import VlcItem 1.0
import QtMultimedia 5.0


ApplicationWindow {
    id: appWindow
    height: 600
    width: 800
    visible: true

    property bool tracking: false
    property bool selecting: false
    property int trackerWidth: 40
    property int trackerHeight: 40
    property int trackerIncrement: 2

    Action {
        shortcut: "W"
        onTriggered: {
            var height = appWindow.trackerHeight + trackerIncrement
            appWindow.trackerHeight = height <= mouseArea.height ? height : mouseArea.height
        }
    }
    
    Action {
        shortcut: "S"
        onTriggered: {
            var height = appWindow.trackerHeight - trackerIncrement
            appWindow.trackerHeight = height >= 0 ? height : 0
        }
    }

    Action {
        shortcut: "A"
        onTriggered: {
            var width = appWindow.trackerWidth + trackerIncrement
            appWindow.trackerWidth = width <= mouseArea.width ? width : mouseArea.width 
        }
    }

    Action {
        shortcut: "D"
        onTriggered: {
            var width = appWindow.trackerWidth - trackerIncrement
            appWindow.trackerWidth = width >= 0 ? width : 0
        }
    }

    Action {
        shortcut: "Ctrl+O"
        onTriggered: {
            openFileDialog.visible = true
        }
    }

    Controller {
        id: ctrl
        // onIsPlayingChanged: console.log("console: playing changed:" + Controller.isPlaying)
    }

    // MediaPlayer {
    //     id: player
    //     source: "file:///Users/zeno/Downloads/PyQt/trailer_480p.mov"
    //     autoPlay: true
    //     onErrorStringChanged: console.log("error:" + errorString)
    // }

    FileDialog {
        id: openFileDialog
        title: "Choose a file for processing."
        onAccepted: {
            console.log("opening " + openFileDialog.fileUrls)
            videoOutput.source = openFileDialog.fileUrls[0]
        }
    }

    toolBar: ToolBar {
        id: navigationBar
        RowLayout {
            anchors.fill: parent;
            ToolButton {
                id: rewindButton
                iconSource: "icons/rewind.png"
                onClicked:  ctrl.rewind()
                enabled: true
            }
            ToolButton {
                id: previousButton
                iconSource: "icons/previous.png"
                onClicked:  ctrl.previous()
                enabled: true
            }
            ToolButton {
                id: playButton 
                iconSource: ctrl.isPlaying ? "icons/pause.png" : "icons/play.png"
                onClicked: ctrl.isPlaying ? ctrl.pause() : ctrl.play()
                enabled: true
            }
            ToolButton {
                id: nextButton
                iconSource: "icons/next.png"
                onClicked:  ctrl.next()
                enabled: true
            }
            ToolButton {
                id: fastForwardButton
                iconSource: "icons/fast-forward.png"
                onClicked:  ctrl.fastForward()
                enabled: true
            }
            Rectangle {
                id: spacer
                Layout.fillWidth: true
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        Text {
            text: qsTr("Here we display the video\nPress 'Ctrl+O' to open a video file.\nClick somewhere into the picture to start tracking.\nUse 'w','a','s','d' keys to increase the size of the tracking region.")
            anchors.centerIn: parent
        }
        Rectangle {
            id: tracker
            z: videoOutput.z+1
            color: "red"
            opacity: 0.5
            visible: appWindow.tracking || appWindow.selecting
            width: appWindow.trackerWidth
            height: appWindow.trackerHeight
            x: mouseArea.mouseX - tracker.width
            y: mouseArea.mouseY - tracker.height
        }
        MouseArea {
            id: mouseArea
            property int pressX: 0
            property int pressY: 0
            anchors.top: videoOutput.top
            anchors.bottom: videoOutput.bottom
            anchors.left: videoOutput.left
            anchors.right: videoOutput.right
            z: videoOutput.z+1
            hoverEnabled: true
            onPositionChanged: {
                if (selecting) {
                    appWindow.trackerWidth = Math.abs(mouseX - pressX)
                    appWindow.trackerHeight = Math.abs(mouseY - pressY)
                }
            }
            onPressed: {
                selecting = true
                pressX = mouseX
                pressY = mouseY
                appWindow.trackerWidth = 0
                appWindow.trackerHeight = 0
            }
            onReleased: {
                selecting = false
                pressX = 0
                pressY = 0

                appWindow.tracking = (appWindow.trackerWidth != 0 || appWindow.trackerHeight != 0)
            }
        }
        VlcItem {
            id: videoOutput
            anchors.fill: parent
        }
        // VideoOutput {
        //     id: videoOutput
        //     source: player
        //     anchors.fill: parent
        // }
    }
}
