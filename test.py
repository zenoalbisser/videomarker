import sys

from PyQt5.QtCore import pyqtProperty, pyqtSignal, pyqtSlot, QObject, QUrl
from PyQt5.QtGui import QGuiApplication
from PyQt5.QtQuick import QQuickView
from PyQt5.QtQml import qmlRegisterType, QQmlApplicationEngine, QQmlComponent


class Controller(QObject):

    isPlayingChanged = pyqtSignal()

    def __init__(self, parent=None):
        super(Controller, self).__init__(parent)

        # Initialise the value of the properties.
        self._playing = False


    # Define getter for the isPlaying property
    @pyqtProperty('bool', notify = isPlayingChanged)
    def isPlaying(self):
        return self._playing

    # # Define the setter of the 'name' property.
    # @isPlaying.setter
    # def isPlaying(self, playing):
    #     self._playing = playing
    #     self.isPlayingChanged.emit()

    @pyqtSlot()
    def rewind(self):
        print 'REWIND'

    @pyqtSlot()
    def previous(self):
        print 'PREVIOUS'

    @pyqtSlot()
    def pause(self):
    	self._playing = False
    	self.isPlayingChanged.emit()
        print 'PAUSE'

    @pyqtSlot()
    def play(self):
    	self._playing = True
    	self.isPlayingChanged.emit()
        print 'PLAY'

    @pyqtSlot()
    def next(self):
        print 'NEXT'

    @pyqtSlot()
    def fastForward(self):
        print 'FAST FORWARD'


app = QGuiApplication(sys.argv)

# Register the Python type.
qmlRegisterType(Controller, 'Controller', 1, 0, 'Controller')

appEngine = QQmlApplicationEngine()
component = QQmlComponent(appEngine)
component.loadUrl(QUrl("main.qml"))

rootObject = component.create()

if rootObject is None:
    # Print all errors that occurred.
    for error in component.errors():
        print(error.toString())

app.exec_()


