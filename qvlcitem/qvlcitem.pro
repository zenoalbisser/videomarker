TEMPLATE = lib
CONFIG += qt plugin
QT += qml quick

# /Applications/VLC.app/Contents/MacOS/lib
LIBS += -l vlc -L$$(VLC_LIBS)

# /Applications/VLC.app/Contents/MacOS/include

INCLUDEPATH += $(VLC_INCLUDES)

DESTDIR = imports/VlcItem
TARGET = qvlcitemplugin
SOURCES += qvlcitem.cpp
HEADERS += qvlcitem.h
