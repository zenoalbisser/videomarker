#include "qvlcitem.h"

#include <QImage>
#include <QPainter>
#include <QMutexLocker>

//----------------------------
#include <vlc/vlc.h>



int done = 0;
uint8_t *videoBuffer = 0;
uint8_t *audioBuffer = 0;
unsigned int videoBufferSize = 0;
unsigned int audioBufferSize = 0;

void cbAudioPrerender (void* p_audio_data, uint8_t** pp_pcm_buffer , unsigned int size)
{
    // FIXME: lock mutex
    if (size > audioBufferSize || !audioBuffer)
    {
        if(audioBuffer) free(audioBuffer);
        audioBuffer = (uint8_t *)malloc(size);
        audioBufferSize = size;
    }
    *pp_pcm_buffer = audioBuffer;
}

void cbAudioPostrender(void* p_audio_data, uint8_t* p_pcm_buffer, unsigned int channels, unsigned int rate, unsigned int nb_samples, unsigned int bits_per_sample, unsigned int size, int64_t pts )
{ }

void cbVideoPrerender(void *p_video_data, uint8_t **pp_pixel_buffer, int size)
{
    // FIXME: lock mutex
    if (size > videoBufferSize || !videoBuffer)
    {
        if(audioBuffer) free(videoBuffer);
        videoBuffer = (uint8_t *)malloc(size);
        videoBufferSize = size;
    }
    *pp_pixel_buffer = videoBuffer;
}

static inline void convertYUVtoRGB(const uint8_t &y, const uint8_t &u, const uint8_t &v, uint8_t &r, uint8_t &g, uint8_t &b)
{
    int16_t rt, gt, bt;
    rt = 1.164 * (y - 16) + 1.596 * (v - 128);
    gt = 1.164 * (y - 16) - 0.813 * (v - 128) - 0.391 * (u - 128);
    bt = 1.164 * (y - 16) + 2.018 * (u - 128);
    r = rt>255? 255 : rt<0 ? 0 : (uint8_t)rt;
    g = gt>255? 255 : gt<0 ? 0 : (uint8_t)gt;
    b = bt>255? 255 : bt<0 ? 0 : (uint8_t)bt;
}

void cbVideoPostrender(void *p_video_data, uint8_t *p_pixel_buffer, int width, int height, int pixel_pitch, int size, int64_t pts)
{
    // printf("cbVideoPostrender size: %i width: %i height: %i pixel_pitch: %i ptc: %lld\n", size, width, height, pixel_pitch, pts);
    // FIXME: unlock mutex
    QImage *frame = new QImage(width, height, QImage::Format_RGB888);
    uint8_t r, g, b;
    uint8_t y, u, v;
    for (int ix = 0; ix < width; ++ix) {
        for (int iy = 0; iy < height; ++iy) {
            y = p_pixel_buffer[ix + width*iy];
            u = p_pixel_buffer[ix + width*iy + width*height];
            v = p_pixel_buffer[ix + width*iy + 2*width*height];

            convertYUVtoRGB(y, u, v, r, g, b);

            frame->setPixel(ix, iy, qRgb(r, g, b));
        }
    }

    QVlcItem* item = (QVlcItem*)p_video_data;
    item->setFrame(frame);
}

static void handleEvent(const libvlc_event_t* pEvt, void* pUserData)
{
    libvlc_time_t time;
    switch(pEvt->type)
    {
        case libvlc_MediaPlayerTimeChanged:
            time = libvlc_media_player_get_time(((QVlcItem*)pUserData)->mediaPlayer());
            // printf("MediaPlayerTimeChanged %lld ms\n", (long long)time);
            break;
        case libvlc_MediaPlayerEndReached:
            // printf ("MediaPlayerEndReached\n");
            done = 1;
            break;
        default:
            printf("%s\n", libvlc_event_type_name(pEvt->type));
    }    
}
//-----------------------------

QVlcItem::QVlcItem(QQuickItem *parent)
	: QQuickPaintedItem(parent)
	, m_frame(0)
	, m_media(0)
    , m_vlcInstance(0)
	, m_media_player(0)
{
    void *pUserData = 0;

    char smem_options[1000];
    sprintf(smem_options
            , "#transcode{vcodec=I444,acodec=s16l}:smem{"
            // , "#transcode{vcodec=mp1v,acodec=s16l}:smem{"
            "video-prerender-callback=%lld,"
            "video-postrender-callback=%lld,"
            "audio-prerender-callback=%lld,"
            "audio-postrender-callback=%lld,"
            "audio-data=%lld,"
            "video-data=%lld},"
            , (long long int)(intptr_t)(void*)&cbVideoPrerender
            , (long long int)(intptr_t)(void*)&cbVideoPostrender
            , (long long int)(intptr_t)(void*)&cbAudioPrerender
            , (long long int)(intptr_t)(void*)&cbAudioPostrender
            , (long long int)this
            , (long long int)this);

    const char * const vlc_args[] = {
        "-I", "dummy", // Don't use any interface
        "--ignore-config", // Don't use VLC's config
        "--extraintf=logger", // Log anything
        "--verbose=1", // Be verbose
        "--sout", smem_options // Stream to memory
    };

    m_vlcInstance = libvlc_new(sizeof(vlc_args) / sizeof(vlc_args[0]), vlc_args);

    m_media_player = libvlc_media_player_new(m_vlcInstance);
    pUserData = this;
    libvlc_event_manager_t* eventManager = libvlc_media_player_event_manager(m_media_player);
    // libvlc_event_attach(eventManager, libvlc_MediaPlayerTimeChanged, handleEvent, pUserData);
    libvlc_event_attach(eventManager, libvlc_MediaPlayerEndReached, handleEvent, pUserData);
    // libvlc_event_attach(eventManager, libvlc_MediaPlayerPositionChanged, handleEvent, pUserData);

}

QVlcItem::~QVlcItem()
{
    libvlc_media_release(m_media);
}

QString QVlcItem::source() const
{
	return m_source;
}

void QVlcItem::setSource(const QString& src)
{
	m_source = QUrl(src).path();
	emit sourceChanged();
	qDebug() << "source changed:" << m_source;


	m_media = libvlc_media_new_path(m_vlcInstance, m_source.toLocal8Bit().data());
    libvlc_media_player_set_media(m_media_player, m_media);
    libvlc_media_player_play(m_media_player);
}

void QVlcItem::setFrame(QImage *frame)
{
	QMutexLocker locker(&m_mutex);
	delete m_frame;
	m_frame = frame;
	// This object lives in a different thread, therefore we have to use a Qt::QueuedConnection
	// to invoke the update() method.
	QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);
}

void QVlcItem::paint(QPainter *painter)
{
	// We should scale and keep aspect ratio
	QMutexLocker locker(&m_mutex);
	if (m_frame)
		painter->drawImage(contentsBoundingRect(), *m_frame);
}

