#ifndef QVLCITEM_H
#define QVLCITEM_H

#include <QtQuick/QQuickPaintedItem>
#include <QtQml/QQmlExtensionPlugin>
#include <QImage>
#include <QMutex>

struct libvlc_media_t;
struct libvlc_instance_t;
struct libvlc_media_player_t;

class QVlcItem : public QQuickPaintedItem
{
	Q_OBJECT
	Q_PROPERTY(QString source READ source WRITE setSource NOTIFY sourceChanged);
public:
    explicit QVlcItem(QQuickItem *parent = 0);
    virtual ~QVlcItem();

    virtual void paint(QPainter *painter) override;

    QString source() const;
    void setSource(const QString& src);

    void setFrame(QImage *frame);

	libvlc_media_player_t *mediaPlayer() { return m_media_player; }


Q_SIGNALS:
	void sourceChanged();

private:
	QString m_source;
	QMutex m_mutex;
	QImage *m_frame;
	libvlc_media_t *m_media;
    libvlc_instance_t *m_vlcInstance;
	libvlc_media_player_t *m_media_player;
};

class QVlcQmlPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QVlcItem")

public:
    void registerTypes(const char *uri)
    {
        Q_ASSERT(uri == QLatin1String("VlcItem"));
        qmlRegisterType<QVlcItem>(uri, 1, 0, "VlcItem");
    }
};

#endif