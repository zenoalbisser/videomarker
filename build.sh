#!/bin/bash


############### Update these paths before building ###################
export VLC_INCLUDES=/Applications/VLC.app/Contents/MacOS/include
export VLC_LIBS=/Applications/VLC.app/Contents/MacOS/lib
export QT_BIN=/Users/zeno/Qt5.2.0/5.2.0/clang_64/bin
export QT_LIBS=/Users/zeno/Qt5.2.0/5.2.0/clang_64/lib

############# DO NOT CHANGE ANYTHING BELOW THIS LINE ##################

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export PATH=${QT_BIN}:${PATH}
cd ${SCRIPT_DIR}/qvlcitem
qmake && make

cd ${SCRIPT_DIR}

#QML2_IMPORT_PATH=/Users/zeno/Downloads/PyQt/videomarker/qvlcitem/imports
#DYLD_LIBRARY_PATH=/Users/zeno/Qt5.2.0/5.2.0/clang_64/lib:/Applications/VLC.app/Contents/MacOS/lib
#PATH=/Users/zeno/Qt5.2.0/5.2.0/clang_64/bin::/Users/zeno/work/webkit_safari/Tools/Scripts:/Users/zeno/work/scripts:/Users/zeno/work/scripts/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/texbin

