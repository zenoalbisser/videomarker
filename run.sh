#!/bin/bash


############### Update these paths before building ###################
export VLC_INCLUDES=/Applications/VLC.app/Contents/MacOS/include
export VLC_LIBS=/Applications/VLC.app/Contents/MacOS/lib
export QT_BIN=/Users/zeno/Qt5.2.0/5.2.0/clang_64/bin
export QT_LIBS=/Users/zeno/Qt5.2.0/5.2.0/clang_64/lib
export PYTHONPATH=$PYTHONPATH:/System/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages

############# DO NOT CHANGE ANYTHING BELOW THIS LINE ##################

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export QML2_IMPORT_PATH=${SCRIPT_DIR}/qvlcitem/imports
export QML_IMPORT_PATH=${SCRIPT_DIR}/qvlcitem/imports
export DYLD_LIBRARY_PATH=${QT_LIBS}:${VLC_LIBS}:${DYLD_LIBRARY_PATH}

echo "PYTHONPATH: ${PYTHONPATH}"
echo "QML2_IMPORT_PATH: ${QML2_IMPORT_PATH}"

python test.py
